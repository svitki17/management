package ru.phone.management;

import com.managment.*;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.phone.management.Model.DeviceConnected;
import ru.phone.management.Model.Enum.StateConnection;
import ru.phone.management.Repository.ConnectedDeviceRepository;

import java.util.Iterator;

@Service
public class TestService {
    @GrpcClient("city-score")
    private CommandCenterGrpc.CommandCenterBlockingStub stub;
    @GrpcClient("city-score")
    private PhoneCenterGrpc.PhoneCenterBlockingStub phoneCenterBlockingStub;


    @Value("${spring.datasource.username}")
    public String test;

    private final ConnectedDeviceRepository deviceRepository;

    public TestService(ConnectedDeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }


    public PhoneResponse getPhone(String number){
        PhoneRequest request = PhoneRequest.newBuilder().setPhoneNumber(number).build();
        Iterator<DeviceConnectedResponse> responseIterator =  phoneCenterBlockingStub.getConnectedDevice(DeviceConnectedRequest.newBuilder().build());
        responseIterator.forEachRemaining(x->{
            DeviceConnected deviceConnected = deviceRepository.findByPhoneNumber(x.getPhoneNumber());
            if (deviceConnected == null) {
                deviceConnected = new DeviceConnected();
                deviceConnected.setPhoneNumber(x.getPhoneNumber());
            }
            deviceConnected.setStateConnection(StateConnection.valueOf(x.getStateConnection()));
            deviceRepository.save(deviceConnected);
        });

        return stub.getCommand(request);

    }
}
