package ru.phone.management.config;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfiguration {
    @Value("${spring.rabbitmq.username}")
    public String userName;

    @Value("${spring.rabbitmq.password}")
    public String password;

    @Value("${spring.rabbitmq.addresses}")
    public String address;
    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setAddresses(address);
        connectionFactory.setUsername(userName);
        connectionFactory.setPassword(password);
        return connectionFactory;
    }

    @Bean
    public AmqpAdmin amqpAdmin(){return  new RabbitAdmin(connectionFactory());
    }

    @Bean
    public Queue queue(){
        return new Queue("test");
    }
    @Bean
    public RabbitTemplate rabbitTemplate() {return new RabbitTemplate(connectionFactory());}


}
