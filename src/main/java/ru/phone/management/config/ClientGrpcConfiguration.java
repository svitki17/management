package ru.phone.management.config;

import com.managment.CommandCenterGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import net.devh.boot.grpc.client.inject.GrpcClientBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.phone.management.TestService;

@Configuration
@GrpcClientBean(
        clazz = CommandCenterGrpc.CommandCenterFutureStub.class,
        beanName = "blockingStub",
        client = @GrpcClient("localhost:5555")
)
public class ClientGrpcConfiguration {


}
