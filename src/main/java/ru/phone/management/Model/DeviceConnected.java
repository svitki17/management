package ru.phone.management.Model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.phone.management.Model.Enum.StateConnection;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "device_connected",schema = "management")
public class DeviceConnected {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "state_connection")
    @Enumerated(EnumType.STRING)
    private StateConnection stateConnection;

    private String ip;

    private String city;

    private String country;

    private String region;

    private String zip;

    private String operator;

    @Column(name = "network_type")
    private String networkType;


}
