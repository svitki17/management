package ru.phone.management.Model;

import com.managment.Command;
import com.managment.StateCommand;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "phone_command",schema = "management")
public class PhoneCommand {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Enumerated(EnumType.STRING)
    private Command command;
    @Column(name = "state_command")
    @Enumerated(EnumType.STRING)
    private StateCommand stateCommand;


}
