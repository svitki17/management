package ru.phone.management.Model.Enum;

public enum StateConnection {
    CONNECTION,

    LOST_CONNECTION,

    DATA_EXCHANGE,
    DOWNTIME
}
