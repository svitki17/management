package ru.phone.management;

import com.managment.CommandCenterGrpc;
import com.managment.PhoneRequest;
import com.managment.PhoneResponse;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
public class RabbitMqController {
    Logger logger = LoggerFactory.getLogger(RabbitMqController.class);
    private final AmqpTemplate template;
    private final TestService testService;
    public RabbitMqController(AmqpTemplate template, TestService testService) {
        this.template = template;
        this.testService = testService;
    }

    @PostMapping("/test")
    public ResponseEntity<String> test(@RequestBody String message){
        logger.info("---send message: {}",message);

        System.out.println(testService.getPhone("test"));

        return ResponseEntity.ok("success");
    }
}
