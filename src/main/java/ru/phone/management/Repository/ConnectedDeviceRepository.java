package ru.phone.management.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.phone.management.Model.DeviceConnected;
@Repository
public interface ConnectedDeviceRepository extends JpaRepository<DeviceConnected, Integer> {
    DeviceConnected findByPhoneNumber(String phoneNumber);
}
