package ru.phone.management.Repository;

import com.managment.Command;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.phone.management.Model.DeviceConnected;
import ru.phone.management.Model.PhoneCommand;

public interface PhoneCommandRepository extends JpaRepository<PhoneCommand, Integer> {
    PhoneCommand findByPhoneNumberAndCommand(String phoneNumber, Command command);
}
