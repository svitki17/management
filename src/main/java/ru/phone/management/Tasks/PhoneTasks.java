package ru.phone.management.Tasks;

import com.managment.*;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.phone.management.Model.DeviceConnected;
import ru.phone.management.Model.Enum.StateConnection;
import ru.phone.management.Model.PhoneCommand;
import ru.phone.management.Repository.ConnectedDeviceRepository;
import ru.phone.management.Repository.PhoneCommandRepository;

import java.util.Iterator;
import java.util.List;

@Component
public class PhoneTasks {
    private static final Logger log = LoggerFactory.getLogger(PhoneTasks.class);
    @GrpcClient("city-score")
    private PhoneCenterGrpc.PhoneCenterBlockingStub phoneCenterBlockingStub;

    @GrpcClient("city-score")
    private CommandCenterGrpc.CommandCenterBlockingStub commandCenterBlockingStub;


    private final ConnectedDeviceRepository deviceRepository;
    private final PhoneCommandRepository commandRepository;

    public PhoneTasks(ConnectedDeviceRepository deviceRepository, PhoneCommandRepository commandRepository) {
        this.deviceRepository = deviceRepository;
        this.commandRepository = commandRepository;
    }

    @Scheduled(fixedDelay = 5000)
    public void getInfoDevices(){

        Iterator<DeviceConnectedResponse> responseIterator =  phoneCenterBlockingStub.getConnectedDevice(DeviceConnectedRequest.newBuilder().build());
        responseIterator.forEachRemaining(x->{
            DeviceConnected deviceConnected = deviceRepository.findByPhoneNumber(x.getPhoneNumber());
            if (deviceConnected == null) {
                deviceConnected = new DeviceConnected();
                deviceConnected.setPhoneNumber(x.getPhoneNumber());
            }
            log.info("-->Number={}, ip={}",deviceConnected.getPhoneNumber(),deviceConnected.getIp());
            deviceConnected.setCity(x.getCity());
            deviceConnected.setCountry(x.getCountry());
            deviceConnected.setOperator(x.getOperator());
            deviceConnected.setRegion(x.getRegion());
            deviceConnected.setPhoneNumber(x.getPhoneNumber());
            deviceConnected.setIp(x.getIp());
            deviceConnected.setZip(x.getZip());
            deviceConnected.setNetworkType(x.getNetworkType());
            deviceConnected.setStateConnection(StateConnection.valueOf(x.getStateConnection()));
            deviceRepository.save(deviceConnected);
        });
    }

    @Scheduled(fixedDelay = 10000)
    public void sendCommands(){
        List<DeviceConnected> deviceConnecteds = deviceRepository.findAll();

        deviceConnecteds.forEach(x->{

            PhoneCommand command = commandRepository.findByPhoneNumberAndCommand(x.getPhoneNumber(),Command.GET_FILES_LIST);
            if(command == null){
                PhoneResponse response = callGrpcMethodSendCommand(x.getPhoneNumber(),Command.GET_FILES_LIST);

                command = new PhoneCommand();
                command.setStateCommand(response.getState());
                command.setCommand(Command.GET_FILES_LIST);
                command.setPhoneNumber(x.getPhoneNumber());
            }
            else if (command.getStateCommand() == StateCommand.COMPLETED){
                PhoneResponse response = callGrpcMethodSendCommand(x.getPhoneNumber(),Command.GET_FILES_LIST);
                command.setStateCommand(response.getState());
            }
            else{
                PhoneResponse response = callGrpcMethodSendCommand(x.getPhoneNumber(),Command.GET_FILES_LIST);
                command.setStateCommand(response.getState());
            }

            commandRepository.save(command);

        });
    }

    public PhoneResponse callGrpcMethodSendCommand(String phoneNumber,Command command){
        PhoneRequest phoneRequest = PhoneRequest.newBuilder()
                .setPhoneNumber(phoneNumber)
                .setCommand(command)
                .build();
        return commandCenterBlockingStub.setProcessingCommand(phoneRequest);
    }
}
